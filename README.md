# BLRevive Community Patch

Relies on server-utils' patching mechanics. Instructions on how to set it up are [available on the wiki](https://blrevive.gitlab.io/wiki/guides/hosting/game-server/server-utils-config/#patches-optional-and-not-recommended).

Currently not compatible with ZCure clients.
